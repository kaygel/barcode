package barcode

import "image/color"

// isItDark takes a color and returns either true of false
func dark(col color.Color) int8 {
	r, g, b, _ := col.RGBA()
	// 65535 / 2 = 32767.5
	mid := uint32(32767)
	if r > mid && g > mid && b > mid {
		return 0
	}
	// log.Println(r, g, b)
	return 1
}

func reduceDark(ia []int8, by int) (nia []int8) {

	for x, eb := range ia {
		if x%by == 0 {
			nia = append(nia, eb)
		}
	}

	return
}
