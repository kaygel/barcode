package barcode

import (
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"path/filepath"
	"testing"
)

func Test_decodeImage(t *testing.T) {
	path, err := filepath.Abs("./examples/upca.png")
	if err != nil {
		t.Fatal(err)
	}
	img, err := decodeImage(path)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(img)

	t.Fail()
}
