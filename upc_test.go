package barcode

import (
	"fmt"
	"log"
	"path/filepath"
	"testing"
)

func TestReadUPCA(t *testing.T) {
	theCode := ReadUPCA()

	// barcode, _ := d.lines[1].toBarcodeString()

	fmt.Println(theCode)
	t.Fail()
}

func TestStuff(t *testing.T) {
	// var ia []int8
	// ia = append(ia, 1)
	// ia = append(ia, 0)
	// ia = append(ia, 1)
	// ia = append(ia, 0)
	// ia = append(ia, 0)
	// ia = append(ia, 0)
	// ia = append(ia, 0)
	// ia = append(ia, 0)
	// ia = append(ia, 0)

	fmt.Println(300 / 95)

	t.Fail()
}

func TestDecoder_Parse(t *testing.T) {
	path, err := filepath.Abs("./examples/DSCF2736-Edit.jpg")
	if err != nil {
		log.Fatal(err)
	}
	img, err := decodeImage(path)
	if err != nil {
		log.Fatal(err)
	}

	d := NewDecoder(*img)

	d.Parse()

	for _, line := range d.lines {
		barcodeData, err := isUPCA(line.darkArray)

		if err != nil {
			continue
		}

		barcode, err := toBarcodeString(barcodeData)

		if err != nil {
			continue
		}

		log.Println(barcodeData)
		log.Println(barcode)
	}

	t.Fail()

}
