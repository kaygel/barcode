package barcode

import (
	"fmt"
	"image"
	"image/color"
	"log"
	"path/filepath"
	"strings"
)

// Decoder holds the image, info about the image and what is in the barcode
// if a barcode is found
type Decoder struct {
	image           image.Gray
	lines           Lines
	dominantBarcode Barcode
	foundBarcodes   map[string]Barcode // the int is count of barcodes found
	result          bool
}

// Barcode holds the barcode data
type Barcode struct {
	symbology string
	data      string
}

// Lines is just a bunch of lines
type Lines []Line

// Line is a line of data that might contain a barcode
// the divisor depends on the fidility of the image to reduce to the on off bits
// in the comments 1 = black 0 = white. In the data true is black white is false
// startAt is the ordinal start of the start flag of a barcode - 101
// middleAt is the ordinal of the start of the middle flag - 01010
// endAt is the ordinal of the start of the end flag of a barcode - 101
// barcodes are 0 00000 00000 0 and each number is 7 bits
// 10100000000000000000000000000000000000000000001010000000000000000000000000000000000000000000101
// total barcode should be (no spaces)
// 101 0000 00000000000000000000 01010 00000000000000000000 101
type Line struct {
	isBarcode         bool
	divisor           int
	barcodeByteString string
	barcodeString     string
	dark              []bool
	darkArray         []int8
}

// upcaRead is a map of the bit strings that equal a UPCA number
// For other symbologies this format will need to be changed since most other
// symoblogies contain much more complicated schemes to allow for both
// left to right and right to left reading
var upcaRead = map[string]string{
	"0001101": "0",
	"1110010": "0",
	"0011001": "1",
	"1100110": "1",
	"0010011": "2",
	"1101100": "2",
	"0111101": "3",
	"1000010": "3",
	"0100011": "4",
	"1011100": "4",
	"0110001": "5",
	"1001110": "5",
	"0101111": "6",
	"1010000": "6",
	"0111011": "7",
	"1000100": "7",
	"0110111": "8",
	"1001000": "8",
	"0001011": "9",
	"1110100": "9",
}

// ReadUPCA reads an image and outputs the UPCA string info
func ReadUPCA() (data string) {
	path, err := filepath.Abs("./examples/upca.png")
	if err != nil {
		log.Fatal(err)
	}
	img, err := decodeImage(path)
	if err != nil {
		log.Fatal(err)
	}

	d := NewDecoder(*img)
	d.Parse()
	d.processLines()

	log.Println(d.lines[1].isBarcode)

	d.foundBarcodes = make(map[string]Barcode)

	count := make(map[string]int)

	for _, line := range d.lines {
		if line.isBarcode {
			d.foundBarcodes[line.barcodeString] = Barcode{
				symbology: "UPCA",
				data:      line.barcodeString,
			}
			count[line.barcodeString]++
		}
	}

	topCount := 0
	for upc, upcCount := range count {
		if upcCount > topCount {
			d.dominantBarcode = d.foundBarcodes[upc]
			topCount = upcCount
		}
	}

	return d.dominantBarcode.data
}

// NewDecoder creates a new decoder
func NewDecoder(img image.Gray) *Decoder {
	d := &Decoder{image: img}
	return d
}

// Parse for barcode
func (d *Decoder) Parse() {
	img := d.image
	bounds := img.Bounds()

	for y := 0; y < bounds.Max.Y; y++ {
		var line Line
		for x := 0; x < bounds.Max.X; x++ {
			col := d.image.At(x, y)
			line.darkArray = append(line.darkArray, dark(col))
		}
		d.lines = append(d.lines, line)
	}
}

// processLines iterates over each line to find a barcode within
func (d *Decoder) processLines() {
	for x := range d.lines {
		minWidth, isBarcode, bitString, barcodeString, err := d.lines[x].processLine()
		if err != nil {
			//
		}
		d.lines[x].isBarcode = isBarcode
		d.lines[x].divisor = minWidth
		d.lines[x].barcodeByteString = bitString
		d.lines[x].barcodeString = barcodeString
	}
}

//
func (l *Line) processLine() (minWidth int, isBarcode bool, bitString string, barcodeString string, err error) {
	shadeLength := 0
	length := len(l.dark)
	for x, ep := range l.dark {
		if x+1 == length {
			continue
		}
		if x == 0 {
			shadeLength++
		} else if ep == l.dark[x-1] {
			shadeLength++
		} else {

			if minWidth > shadeLength || minWidth == 0 {
				minWidth = shadeLength
			}
			if ep == false {
				shadeLength = shadeLength * -1
			}
			shadeLength = 1
		}

	}

	//
	reducedLine := l.reduceBy(minWidth)

	bitString, err = returnBarcodeBytes(reducedLine.toBytes())
	if err != nil {
		return minWidth, false, bitString, barcodeString, err
	}

	barcodeString, err = toBarcodeString(bitString)

	isBarcode = true

	return
}

func isUPCA(ia []int8) (barcode string, err error) {
	len := len(ia)

	maxDivisor := len / 95

	for x := 1; x < maxDivisor+1; x++ {
		nia := reduceDark(ia, x)
		barcode, err := checkForUPCA(nia, len/x)
		if err == nil {
			return barcode, nil
		}
	}

	return "", fmt.Errorf("No Barcode found withing string")
}

func checkForUPCA(ia []int8, len int) (barcode string, err error) {

	if len < 95 {
		return "", fmt.Errorf("Array too small to contain a barcode")
	}
	for x := range ia {
		// if x + barcode length is greater than the array this is not a barcode
		if x+95 > len {
			return "", fmt.Errorf("No Barcode found withing string")
		}
		// convert the parts to check if start, middle and end are right
		start := byteToString(ia[x : x+3])
		middle := byteToString(ia[x+45 : x+45+5])
		end := byteToString(ia[x+92 : x+92+3])

		if start == "101" && middle == "01010" && end == "101" {
			return byteToString(ia[x : x+95]), nil
		}
	}
	return "", fmt.Errorf("No Barcode found withing string")
}

func returnBarcodeBytes(ia []int8) (barcode string, err error) {
	// Should get a start then
	// 101 000000000000000000000000 01010 00000000000000000000 0000 101
	// 10100000000000000000000000001010000000000000000000000000101
	// 10100011010010011001001101101110010011011101101010110011011011001100110110011011001101110100101000000000
	// 10100000000000000000000000000000000000000000001010000000000000000000000000000000000000000000101
	len := len(ia)
	for x := range ia {
		// if x + barcode length is greater than the array this is not a barcode
		if x+95 > len {
			return "", fmt.Errorf("No Barcode found withing string")
		}
		// convert the parts to check if start, middle and end are right
		start := byteToString(ia[x : x+3])
		middle := byteToString(ia[x+45 : x+45+5])
		end := byteToString(ia[x+92 : x+92+3])

		if start == "101" && middle == "01010" && end == "101" {
			return byteToString(ia[x : x+95]), nil
		}
	}
	return "", fmt.Errorf("No Barcode found withing string")
}

// toBarcodeString takes a string that should represent the 0's and 1's of a
// barcode and convert those (using a map) to a string that is the UPC numbers
func toBarcodeString(barcodeByteString string) (barcode string, err error) {
	// make an array to hold the strings in
	var barcodeArray []string
	// just to make things easier in typeing down the line while still keeping
	// the base name descriptive
	bbs := barcodeByteString
	// unless the string is 95 characters this is not a UPCA
	if len(bbs) == 95 {
		for x := 3; x < 92; x = x + 7 {
			if x == 45 {
				x = x + 5
			}
			singleNumber := upcaRead[bbs[x:x+7]]
			if singleNumber != "" {
				barcodeArray = append(barcodeArray, upcaRead[bbs[x:x+7]])
			} else {
				return "", fmt.Errorf("Some data in the barcode did not decode")
			}
		}
	} else {
		return "", fmt.Errorf("The string was not long enough to be a barcode")
	}
	return strings.Join(barcodeArray, ""), nil
}

// toBytes converts the dark bool value to a 1 or 0
func (l *Line) toBytes() (intArray []int8) {
	for _, b := range l.dark {
		intArray = append(intArray, boolToInt(b))
	}
	return
}

// helper function so toByteString can be a method and use byteToString in other
// areas
func (l *Line) toByteString() (str string) {
	intArray := l.toBytes()
	return byteToString(intArray)
}

// byteToString converts the array of 0's and 1's to a string
// essentiall strings.Join for ints
func byteToString(ia []int8) (str string) {
	str = strings.Trim(strings.Join(strings.Fields(fmt.Sprint(ia)), ""), "[]")
	return
}

// boolToInt is super simple, converts a bool value to either a 0 or 1
func boolToInt(b bool) (i int8) {
	if b {
		i = 1
	}
	return
}

// reduceLine divides the line @ n by by, needed so that the resulting
// reduction is in the Decoder
func (d *Decoder) reduceLine(n int, by int) {
	d.lines[n] = d.lines[n].reduceBy(by)
}

// reduceBy reduces the amount of dark bools by the amount passed
func (l *Line) reduceBy(n int) (newLine Line) {
	if n == 0 {
		return *l
	}
	for x, el := range l.dark {
		if x%n == 0 {
			newLine.dark = append(newLine.dark, el)
		}
	}

	return newLine
}

// isItDark takes a color and returns either true of false
func isItDark(col color.Color) bool {
	r, g, b, _ := col.RGBA()
	// 65535 / 2 = 32767.5
	mid := uint32(32767)
	if r > mid && g > mid && b > mid {
		return false
	}
	// log.Println(r, g, b)
	return true
}
